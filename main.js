var audioMetadata = {};
var metadata = {};

var config = {
  databaseURL: "https://fillydelphia-radio.firebaseio.com",
  projectId: "fillydelphia-radio"
};

function htmlDecode(input) {
  var e = document.createElement("div");
  e.innerHTML = input;
  // handle case of empty input
  return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}
firebase.initializeApp(config);
var database = firebase.database();
var nowPlaying = firebase.database().ref("rest/fillydelphia-radio/now-playing");

nowPlaying.on("value", function(snapshot) {
  audioMetadata = {
    artist: processMeta(snapshot).artist,
    title: processMeta(snapshot).title,
    album: processMeta(snapshot).album
  };
  metadata.artist = snapshot.val().artist
    ? htmlDecode(snapshot.val().artist)
    : null;
  metadata.album = snapshot.val().album
    ? htmlDecode(snapshot.val().album)
    : null;
  metadata.track = snapshot.val().title
    ? htmlDecode(snapshot.val().title)
    : null;
  metadata.coverUrl =
    "https://fillyradio.com/cover?nocache=" + new Date().getTime();
  setMetaText(audioMetadata);
});

function processMeta(snapshot) {
  var results = snapshot.val();
  if (results.title != null) {
    var track = results.title;
  } else {
    var track = "Unknown Track";
  }
  if (results.artist != null) {
    var artist = results.artist;
  } else {
    var artist = "Unknown Artist";
  }
  if (results.album != null && results.album != results.title) {
    var album = results.album;
  } else if (results.album == results.title) {
    var album = results.album + " - Single";
  } else {
    var album = "Unknown Album";
  }
  return { title: track, artist: artist, album, album };
}

function setMetaText(m) {
  jQuery(".artist").html(m.artist);
  jQuery(".title").html(m.title);
  jQuery(".album").html("from <em>" + m.album + "</em>");
  axios
    .get("https://fillyradio.com/cover/500?" + new Date().getTime(), {
      responseType: "blob"
    })
    .then(function(response) {
      var reader = new window.FileReader();
      reader.readAsDataURL(response.data);
      reader.onload = function() {
        var imageDataUrl = reader.result;
        jQuery(".art > img").attr("src", imageDataUrl);
      };
    })
    .catch(function(e) {
      console.log(e);
      jQuery(".art > img").attr(
        "src",
        "https://res.cloudinary.com/stormdragon-designs/image/upload/c_scale,q_auto,w_300,h_300/v1526676139/fricon_600-600_zersgq.png"
      );
    });
}
